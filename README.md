Hi, I am an internet marketer, I want to suggest that you get acquainted with diving. Our website http://dipndive.com  has equipment and tips for this sport. If anyone knows, diving - scuba diving, using scuba diving, masks, flippers and wetsuits. 
It is very common in seaside resorts, and also recently it has been practiced on lakes, rivers, and even in pools. However, I will not dwell on the listing of places to practice and will still talk about sea diving. I have to say, you will have your 
first dive with an instructor. This is mandatory, and for your own good. Now let's talk about the pros and cons of this entertainment.
Pros:
1. Adrenaline
Maybe diving seems calm and easy to do, but it is not. When you dive, you will feel how your body, caught in a foreign environment for itself, will produce adrenaline. So after you swim out, you will still feel excited for a long time.
2. Aesthetics
When you swim under water, a wonderful view of the marine flora and fauna. As a rule, the guides try to take you to the most picturesque place so that you feel even more pleasure from diving.
3. Pleasant aftertaste
In my opinion - diving is an extreme sport. Therefore, after you finish the dive, you can be quite proud of yourself that you made such a dangerous journey.
4. Efficiency
Diving is not a costly business and the cost of renting costumes is not expensive. Therefore, you do not need to be an oligarch to afford this entertainment.
Now about the minuses:
1. High pressure
For people with heart problems, it is best to abstain from diving. At depth, as you know, there is pressure. That is why the instructor plunges with you, who makes sure that no one feels bad.
2. Take a long time
As a rule, dive sites are carefully chosen and in most cases, you have to go far there. However, it is worth it, because it's all the more pleasant to see not just the blue, but the beautiful underwater world.
I highly recommend you to get acquainted with diving. So when you go to a seaside resort, remember this article and my recommendations.




